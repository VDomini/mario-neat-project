A full writeup for this project is located at NEATMario.pdf.

An example run of an agent that completed difficulty 3 can be seen at https://www.youtube.com/watch?v=GOndCIAqLgk&feature=youtu.be

This code needs to be run with Java version 1.6, in my experience versions 1.7 and 1.8 will throw errors.

The code for the project is located at trunk\src\ch\idsia.
The code that I have created is located at two classes: 
	ch\idsia\agents\controllers\AluiseNEATAgent.java
	ch\idsia\scenarios\NEAT.java

AluiseNEATAgent contains the implementation for one agent: so the Artificial Neural Network, getting Outputs from inputs, mutation, genetic algorithm.

NEAT contains the overall evolutionary algorithm for testing fitness of generations, creating new ones, and running the code.

You can run ch.idsia.scenarios.NEAT to achieve the same results. However, the run time for this program as is, is on the order of hours.

On every 100th generation, the Agent is saved to a file underneath AgentResults in the difficulty for the given level. 
If an Agent completes the level, it is saved to AgentResults/SuccessfulAgents.

These files can be played using ch.idsia.scenarios.Play, by editing the code of the file where specified to contain the filepath to the agent. Make sure the difficulty is changed too. (When submitted it will show an agent completing level 1).

Finally, the fitnesses (Best, Second, 10th Percentile) for each generation for each difficulty will be saved to a csv file at difficultyX 