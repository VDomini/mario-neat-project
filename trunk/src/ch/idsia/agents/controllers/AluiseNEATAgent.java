package ch.idsia.agents.controllers;

import java.util.Random;

import ch.idsia.agents.Agent;
import ch.idsia.benchmark.mario.engine.sprites.Mario;
import ch.idsia.benchmark.mario.environments.Environment;

//This is the AluiseNEATAgent class, which controls the ANN associated with each Agent

public class AluiseNEATAgent extends BasicMarioAIAgent implements Agent{
	
	static private final String name = "AluiseNEATAgent";
	
	//Number of inputs is the area of the box around mario (9x9) + 3(On ground, can jump, bias)
	final static int numInputs = 84;
	
	//Number of outputs = number of keys that can be pressed
	final static int numOutputs = Environment.numberOfKeys;
	
	//Number of hidden nodes in the 2 node layers
	final static int numHidden = 20;
	
	//These are the connections between layers
	
	//From input to output
	protected double[][] directConnectionLayer;
	
	//Input to nodes to output
	protected double[][] firstConnectionLayer;
	protected double[][] recurrentConnectionLayer;
	protected double[][] secondConnectionLayer;
	
	//Output array
	protected double[] outputs;
	
	//Mutation magnitude. The random number used to modify the edge weights will be multiplied by this
	protected static double mutationMagnitude = 0.3;

	private final static Random random = new Random();
	
	//Initial creation method. This initializes everything and calls mutate to create random connections.
	public AluiseNEATAgent()
	{
	    super(name);
	    this.directConnectionLayer = new double[numInputs][numOutputs];
	    this.firstConnectionLayer = new double[numInputs][numHidden];
	    this.recurrentConnectionLayer = new double[numHidden][numHidden];
	    this.secondConnectionLayer = new double[numHidden][numOutputs];
	    this.outputs = new double[numOutputs];
	    
	    this.mutate();
	}
	
	//This is for the breeding method to create a child using their instantiated arrays
	public AluiseNEATAgent(double[][] a, double[][] b, double[][] c, double[][] d)
	{
		super(name);
	    this.firstConnectionLayer = a;
	    this.recurrentConnectionLayer = b;
	    this.secondConnectionLayer = c;
	    this.directConnectionLayer = d;
	    this.outputs = new double[numOutputs];
	}
	
	//The primary driver of change and evolution in the system.
	private void mutate(){
		//The number of mutations is random between 1 and 2
		int numinitcons = random.nextInt(2) + 1;
		
		for( int i = 0; i < numinitcons; i++)
		{
			
			int a = random.nextInt(numInputs);
			int b = random.nextInt(numHidden);
			int c = random.nextInt(numHidden);
			int d = random.nextInt(numOutputs);
			
			int dORn = random.nextInt(100);
			
			//80 percent chance a direct connection is added/modified, 20 percent chance a node connection is added/modified
			if(dORn < 80)
			{
				this.directConnectionLayer[a][d] = this.directConnectionLayer[a][d] + random.nextGaussian();
			}
			else
			{
				this.firstConnectionLayer[a][b] = this.firstConnectionLayer[a][b] + random.nextGaussian();
				this.recurrentConnectionLayer[b][c] = this.recurrentConnectionLayer[b][c] + random.nextGaussian();
				this.secondConnectionLayer[c][d] = this.secondConnectionLayer[c][d] + random.nextGaussian();
			}
		}
		
		//Number of mutations random between 0 and 2
		int numchanges = random.nextInt(3);
		for( int i = 0; i < numchanges; i++)
		{
			int sel = random.nextInt(4);
			//25% chance a direct connection is swapped
			if( sel < 1)
			{
				int e = random.nextInt(numInputs);
				int f = random.nextInt(numOutputs);
				int g = random.nextInt(numOutputs);
				
				double hold = this.directConnectionLayer[e][f];
				this.directConnectionLayer[e][f] = this.directConnectionLayer[e][g];
				this.directConnectionLayer[e][g] = hold;
			}
			else //75% chance a node connection is swapped
			{
				int h = random.nextInt(numHidden);
				int k = random.nextInt(numOutputs);
				int l = random.nextInt(numOutputs);
				double hold2 = this.secondConnectionLayer[h][k];
				this.secondConnectionLayer[h][k] = this.secondConnectionLayer[h][l];
				this.secondConnectionLayer[h][l] = hold2;
			}
			
		}
		
	}
	
	//This is the function required in every Agent class which will get the output buttons to press
	public boolean[] getAction()
	{
	    byte[][] scene = mergedObservation;
	    double[] inputs = new double[numInputs];

	    int which = 0;
	    
	    //This gets the array of elements surrounding mario for the input array
	    for (int i = -4; i < 5; i++)
	    {
	        for (int j = -4; j < 5; j++)
	        {
	            inputs[which++] = probe(i, j, scene);
	        }
	    }
	    inputs[inputs.length - 3] = isMarioOnGround ? 1 : 0;
	    inputs[inputs.length - 2] = isMarioAbleToJump ? 1 : 0;
	    inputs[inputs.length - 1] = 1;
	    
	    //This runs propagate with the inputs to get the outputs
	    double[] outputs = propagate(inputs);
	    
	    //If the value for the output returned is greater than 0, perform that action
	    for (int i = 0; i < action.length; i++)
	    {
	        action[i] = outputs[i] > 0;
	    }
	    //This needs to be here because in this game, if you hold the jump button, Mario won't repeatedly jump
	    if(!isMarioAbleToJump && isMarioOnGround)
	    {
	    	action[Mario.KEY_JUMP] = false;
	    }
	    return action;
	}

	//The real x,y value in the grid around mario is offset by 11
	private double probe(int x, int y, byte[][] scene)
	{
	    int realX = x + 11;
	    int realY = y + 11;
	    return (scene[realX][realY] > 0) ? 1 : (scene[realX][realY] == 0) ? 0 : -1;
	}
	
	//PROPAGATE
	private double[] propagate(double[] inputs)
	{
		double[] out = new double[numOutputs];
		double[] mid = new double[numHidden];
		
		//Propagate first connection layer by matrix multiplication
		for (int i = 0; i < numInputs; i++) { // aRow
            for (int j = 0; j < numHidden; j++) { // bColumn
                    mid[j] += inputs[i] * this.firstConnectionLayer[i][j];
            }
        }
		
		//Take the tanh to get a value between -1 and 1
		for( int i = 0; i < mid.length; i++)
		{
			mid[i] = Math.tanh(mid[i]);
		}
		
		//Propagate the second connection layer
		double[] mid2 = new double[numHidden];
		for (int i = 0; i < numHidden; i++) { // aRow
            for (int j = 0; j < numHidden; j++) { // bColumn
                mid2[j] += mid[i] * this.recurrentConnectionLayer[i][j];
            
            }
        }
		
		//tanh again
		for( int i = 0; i < mid2.length; i++)
		{
			mid2[i] = Math.tanh(mid2[i]);
		}
		
		//Propagate from mid2 to outputs
		for (int i = 0; i < numHidden; i++) { // aRow
            for (int j = 0; j < numOutputs; j++) { // bColumn
                out[j] += mid2[i] * this.secondConnectionLayer[i][j];
            
            }
        }
		
		//Now do the direct connection layer and add those values in
		for (int i = 0; i < numInputs; i++) { // aRow
            for (int j = 0; j < numOutputs; j++) { // bColumn
            	out[j] += inputs[i]* this.directConnectionLayer[i][j];
            }
		}
		
		//Finally, tanh everything again
		for( int i = 0; i < out.length; i++)
		{
			out[i] = Math.tanh(out[i]);
		}
		
		return out;
	}


	public Agent New() {
		// TODO Auto-generated method stub
		return new AluiseNEATAgent();
	}
	
	//Genetic Algorithm
	public static Agent breed(AluiseNEATAgent a, AluiseNEATAgent b){
		double[][] newDLayer = new double[numInputs][numOutputs];
		double[][] newCLayer = new double[numInputs][numHidden];
		double[][] newRCLayer = new double[numHidden][numHidden];
		double[][] newSCLayer = new double[numHidden][numOutputs];
		
		//In essence here we are copying over each member array to a new child.
		//For each element, there is a 20% chance it will be mutated. 
		//If the two parents have a nonzero connection, the child's connection will be chosen at random
		//Finally, the child is created and then has a 20% chance to be further mutated
		
		for(int i = 0; i < numInputs; i++)
		{
			for( int j = 0; j < numOutputs; j++)
			{
				int mut = random.nextInt(10);
				double mutamount = 0;
				if( mut < 2)
				{
					mutamount += random.nextGaussian()*mutationMagnitude;
				}
				if( a.directConnectionLayer[i][j] != 0 && b.directConnectionLayer[i][j] != 0 && a.directConnectionLayer[i][j] == b.directConnectionLayer[i][j])
				{
					newDLayer[i][j] = a.directConnectionLayer[i][j] + mutamount;
				}
				else if( a.directConnectionLayer[i][j] != 0 && b.directConnectionLayer[i][j] != 0 && a.directConnectionLayer[i][j] != b.directConnectionLayer[i][j])
				{
					int r = random.nextInt(2);
					if( r == 0){
						newDLayer[i][j] = a.directConnectionLayer[i][j] + mutamount;
					}
					else{
						newDLayer[i][j] = b.directConnectionLayer[i][j] + mutamount;
					}
				}
				else if( a.directConnectionLayer[i][j] == 0 && b.directConnectionLayer[i][j] == 0)
				{
					newDLayer[i][j] = 0;
				}
				else{
					newDLayer[i][j] = a.directConnectionLayer[i][j] + b.directConnectionLayer[i][j] + mutamount;
				}
			}
		}
		
		
		for(int i = 0; i < a.firstConnectionLayer.length; i++)
		{
			for(int j = 0; j < a.firstConnectionLayer[0].length; j++)
			{
				int mut = random.nextInt(10);
				double mutamount = 0;
				if( mut < 2)
				{
					mutamount += random.nextGaussian()*mutationMagnitude;
				}
				if( a.firstConnectionLayer[i][j] != 0 && b.firstConnectionLayer[i][j] != 0 && a.firstConnectionLayer[i][j] == b.firstConnectionLayer[i][j])
				{
					newCLayer[i][j] = a.firstConnectionLayer[i][j] + mutamount;
				}
				else if( a.firstConnectionLayer[i][j] != 0 && b.firstConnectionLayer[i][j] != 0 && a.firstConnectionLayer[i][j] != b.firstConnectionLayer[i][j])
				{
					int r = random.nextInt(2);
					if( r == 0){
						newCLayer[i][j] = a.firstConnectionLayer[i][j] + mutamount;
					}
					else{
						newCLayer[i][j] = b.firstConnectionLayer[i][j] + mutamount;
					}
				}
				else if( a.firstConnectionLayer[i][j] == 0 && b.firstConnectionLayer[i][j] == 0)
				{
					newCLayer[i][j] = 0;
				}
				else{
					newCLayer[i][j] = a.firstConnectionLayer[i][j] + b.firstConnectionLayer[i][j] + mutamount;
				}
			}
		}
		
		for(int i = 0; i < a.recurrentConnectionLayer.length; i++)
		{
			for(int j = 0; j < a.recurrentConnectionLayer[0].length; j++)
			{
				
				int mut = random.nextInt(10);
				double mutamount = 0;
				if( mut < 2)
				{
					mutamount += random.nextGaussian()*mutationMagnitude;
				}
				if( a.recurrentConnectionLayer[i][j] != 0 && b.recurrentConnectionLayer[i][j] != 0 && a.recurrentConnectionLayer[i][j] == b.recurrentConnectionLayer[i][j])
				{
					newRCLayer[i][j] = a.recurrentConnectionLayer[i][j] + mutamount;
				}
				else if( a.recurrentConnectionLayer[i][j] != 0 && b.recurrentConnectionLayer[i][j] != 0 && a.recurrentConnectionLayer[i][j] != b.recurrentConnectionLayer[i][j])
				{
					int r = random.nextInt(2);
					if( r == 0){
						newRCLayer[i][j] = a.recurrentConnectionLayer[i][j] + mutamount;
					}
					else{
						newRCLayer[i][j] = b.recurrentConnectionLayer[i][j] + mutamount;
					}
				}
				else if( a.recurrentConnectionLayer[i][j] == 0 && b.recurrentConnectionLayer[i][j] == 0)
				{
					newRCLayer[i][j] = 0;
				}
				else{
					newRCLayer[i][j] = a.recurrentConnectionLayer[i][j] + b.recurrentConnectionLayer[i][j] + mutamount;
				}
			}
		}
		
		for(int i = 0; i < a.secondConnectionLayer.length; i++)
		{
			for(int j = 0; j < a.secondConnectionLayer[0].length; j++)
			{
				int mut = random.nextInt(10);
				double mutamount = 0;
				if( mut < 2)
				{
					mutamount += random.nextGaussian()*mutationMagnitude;
				}
				if( a.secondConnectionLayer[i][j] != 0 && b.secondConnectionLayer[i][j] != 0 && a.secondConnectionLayer[i][j] == b.secondConnectionLayer[i][j])
				{
					newSCLayer[i][j] = a.secondConnectionLayer[i][j] + mutamount;
				}
				else if( a.secondConnectionLayer[i][j] != 0 && b.secondConnectionLayer[i][j] != 0 && a.secondConnectionLayer[i][j] != b.secondConnectionLayer[i][j])
				{
					int r = random.nextInt(2);
					if( r == 0){
						newSCLayer[i][j] = a.secondConnectionLayer[i][j] + mutamount;
					}
					else{
						newSCLayer[i][j] = b.secondConnectionLayer[i][j] + mutamount;
					}
				}
				else if( a.secondConnectionLayer[i][j] == 0 && b.secondConnectionLayer[i][j] == 0)
				{
					newSCLayer[i][j] = 0;
				}
				else{
					newSCLayer[i][j] = a.secondConnectionLayer[i][j] + b.secondConnectionLayer[i][j] + mutamount;
				}
			}
		}
		
		int r2 = random.nextInt(10);
		
		
		AluiseNEATAgent out = new AluiseNEATAgent(newCLayer, newRCLayer, newSCLayer, newDLayer);
		
		if( r2 > 8){
			out.mutate();
		}
		
		return out;
		
	}

}
