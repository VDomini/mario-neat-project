package ch.idsia.scenarios;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.idsia.agents.Agent;
import ch.idsia.agents.controllers.AluiseNEATAgent;
import ch.idsia.agents.learning.MediumSRNAgent;
import ch.idsia.benchmark.mario.engine.GlobalOptions;
import ch.idsia.benchmark.tasks.ProgressTask;
import ch.idsia.benchmark.tasks.Task;
import ch.idsia.evolution.Evolvable;
import ch.idsia.evolution.ea.ES;
import ch.idsia.tools.MarioAIOptions;
import ch.idsia.utils.wox.serial.Easy;


//This is the main class for running the Evolution algorithm
public class NEAT {
	//Number of generations
	final static int generations = 100000;
	
	//The population per generation
	final static int populationSize = 1000;
	final static int numSpecies = 10;
	final static int speciesSize = populationSize/numSpecies;
	
	
	//The number of total difficulties
	final static int numdifficulties = 11;
	
	//This array holds all agents in the current population
	private static Agent[][] population;
	
	//This array keeps track of the fitness of each agent in the current population
	private static float[][] fitness;
	private static int elite;
	private static Task task;
	
	//This keeps track of the overall fitness for graphing purposes
	private static double[][][] fitnessTotal;
	private static int[] genfinish;
	
	private final static Random random = new Random();
	
	//This creates the initial population, sets the level task, and associated variables
	public static void CreatePopulation(Task t, AluiseNEATAgent initial, int populationSize)
	{
	    population = new Agent[numSpecies][speciesSize];
	    for( int j = 0; j < numSpecies; j++)
	    {
	    	for (int i = 0; i < speciesSize; i++)
	    	{
	    		population[j][i] = initial.New();
	    	}
	    }
	    fitness = new float[numSpecies][speciesSize];
	    fitnessTotal = new double[numdifficulties][generations][3];
	    genfinish = new int[numdifficulties];
	    for(int i = 0; i < numdifficulties; i++)
	    {
	    	genfinish[i] = generations;
	    }
	    //This sets the elite members to be the upper half of the population
	    elite = speciesSize / 2;
	    task = t;
	}

	//This function evaluates all members of the current generation by running them against the level
	public static void evalGeneration()
	{
	    for (int i = 0; i < numSpecies; i++)
	    {
	    	for(int j = 0; j < speciesSize; j++)
	    	{
	    		evaluate(i,j);
	    	}
	    }
	    sortPopulationByFitness();
	}
	
	//This function creates the new generation
	public static void newGen()
	{
		for(int k = 0; k < numSpecies; k++)
		{
			Agent[] newpopulation = new Agent[speciesSize];
			//We pass through the top elite/5 (10%
			for( int i = 0; i < elite/5; i++)
			{
				newpopulation[i] = population[k][i];
			}
			//The remaining 90% will be produced as offspring of the elite.
			for( int i = elite/5; i < speciesSize; i++)
			{
				int father = random.nextInt(elite);
				int mother = random.nextInt(elite);
				while( mother == father){
					mother = random.nextInt(elite);
				}
				
				int difSpecMom = random.nextInt(10);
				int momSpec = k;
				if( difSpecMom < 3)
				{
					while( momSpec == k)
					{
						momSpec = random.nextInt(numSpecies);
					}
				}
				//Ensure more fit parent is called first
				if( father < mother){
					newpopulation[i] = AluiseNEATAgent.breed((AluiseNEATAgent)population[k][father], (AluiseNEATAgent)population[momSpec][mother]);
				}
				else
				{
					newpopulation[i] = AluiseNEATAgent.breed((AluiseNEATAgent)population[momSpec][mother], (AluiseNEATAgent)population[k][father]);
				}
			}
			//Set current population to new population
			population[k] = newpopulation;
		}
	}

	//The evaluation function. This runs the agent through the task (level) and returns the distance it travelled.
	private static void evaluate(int i, int j)
	{
	    fitness[i][j] = 0;
	    population[i][j].reset();
	    fitness[i][j] += task.evaluate((Agent) population[i][j]);
	    
	}

	//This sorts the opulation by fitness, essential for gathering the elite members
	private static void sortPopulationByFitness()
	{
		for( int k = 0; k < numSpecies; k++)
		{
			for (int i = 0; i < speciesSize; i++)
			{
				for (int j = i + 1; j < speciesSize; j++)
				{
					if (fitness[k][i] < fitness[k][j])
					{
						swap(k, i, j);
					}
				}
			}
		}
	}

	//Helper function for sort that swaps Agents and fitnesses
	private static void swap(int k, int i, int j)
	{
	    float cache = fitness[k][i];
	    fitness[k][i] = fitness[k][j];
	    fitness[k][j] = cache;
	    Agent gcache = population[k][i];
	    population[k][i] = population[k][j];
	    population[k][j] = gcache;
	}

	//Gets the best agent
	public static Agent[] getBests()
	{
		int bestfit = 0;
		double maxfit = 0;
		for( int i = 0; i < numSpecies; i++)
		{
			if( fitness[i][0] > maxfit)
			{
				maxfit = fitness[i][0];
				bestfit = i;
			}
		}
	    return new Agent[]{population[bestfit][0]};
	}

	//Gets the best agents fitness
	public static float[] getBestFitnesses()
	{
		int bestfit = 0;
		double maxfit = 0;
		for( int i = 0; i < numSpecies; i++)
		{
			if( fitness[i][0] > maxfit)
			{
				maxfit = fitness[i][0];
				bestfit = i;
			}
		}
	    return new float[]{fitness[bestfit][0]};
	}
	
	public static void cullSpecies()
	{
		int bestfit = 0;
		double maxfit = 0;
		for( int i = 0; i < numSpecies; i++)
		{
			if( fitness[i][0] > maxfit)
			{
				maxfit = fitness[i][0];
				bestfit = i;
			}
		}
		int cull = random.nextInt(numSpecies);
		while(cull == bestfit)
		{
			cull = random.nextInt(numSpecies);
		}
		
		AluiseNEATAgent initial = new AluiseNEATAgent();
		for (int i = 0; i < speciesSize; i++)
    	{
    		population[cull][i] = initial.New();
    	}
	}
	

	public static void main(String[] args)
	{
	    MarioAIOptions options = new MarioAIOptions(args);
	    String sfolname = "AgentResults/SuccessfulAgents/";
	    
	    //Loop through each difficulty
	    for (int difficulty = 4; difficulty < numdifficulties; difficulty++)
	    {
	        System.out.println("New Evolve phase with difficulty = " + difficulty + " started.");
	        
	        //Initialize new agent
	        AluiseNEATAgent initial = new AluiseNEATAgent();
	        
	        //Set level difficulty
	        options.setLevelDifficulty(difficulty);
	        
	        //Set new agent
	        options.setAgent(initial);

	        options.setFPS(GlobalOptions.MaxFPS);
	        options.setVisualization(false);

	        //Set the task to be progressing through level (distance)
	        Task task = new ProgressTask(options);
	        String folderName = "AgentResults/Difficulty"+difficulty + "/";
	        
	        //Create the initial population
	        CreatePopulation(task, initial, populationSize);

	        int gen = 0;
	        while( gen < generations)
	        {
	        	//Evaluate the current generation
	            evalGeneration();
	            
	            //Get the best, second best, and 10% percentile best of each generation
	            
	            //If a run is in a gen % 5 == 0 generation or beats the level, display i
	            
	            //Check if the agent beat the level
	            Agent a = (Agent) getBests()[0];
	            double bestResult = getBestFitnesses()[0];
	            fitnessTotal[difficulty][gen][0] = bestResult;
	            System.out.println("Generation " + gen + " best " + bestResult);
	            //options.setVisualization(gen % 5 == 0 || bestResult > 4000);
	            double result = task.evaluate(a);
	            options.setVisualization(false);
	            
	            
	            
	            //Every tenth generation, save the agent to file.
	            String fname = "20evolved"+difficulty+"-"+gen + ".xml";
	            if( gen % 1000 == 0)
	            {
	            	Easy.save(getBests()[0], folderName+fname);
	            	if( gen > 0)
	            	{
	            		cullSpecies();
	            	}
	            }
	            gen++;
	            //If the agent beat the level, save it to file and store the generation number
	            if (result > 4000)
	            {
	            	Easy.save(getBests()[0], sfolname+fname);
	            	genfinish[difficulty] = gen;
	                break; // Go to next difficulty.
	            }
	            newGen();
	        }
	        
	        //Save the fitnesses to a csv file
	        PrintWriter pw;
			try {
		        	pw = new PrintWriter(new File("20difficulty"+difficulty+".csv"));
					StringBuilder sb = new StringBuilder();
		        	for( int j = 0; j < genfinish[difficulty]; j++)
		        	{
		        		sb.append(""+fitnessTotal[difficulty][j][0]);
		        		sb.append('\n');
		        	}
		        	pw.write(sb.toString());
		            pw.close();
		        
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
	    System.exit(0);
	}
}
